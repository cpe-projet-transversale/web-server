start:
	docker-compose up -d
	. venv/bin/activate && \
	export FLASK_APP=main.py && \
	flask run

install:
	python3 -m venv venv
	. venv/bin/activate
	pip install Flask