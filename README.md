# Installation

## Database

Start Postgres Database  and PostgresMyAdmin

``` bash
docker-compose up
```

Recreate/Reset DB with script folder

``` bash
docker-compose up --force-recreate --renew-anon-volumes
```

Go to pgAdmin at http://localhost:5050
1) Login admin/admin
2) Add new server
3) name: DB
4) Connexion -> host: postgres
5) Connexion -> user: postgres
6) Connexion -> password: password

## Install Flask and Python Env

``` bash
    make install
```

# Start application

``` bash
    make start
```